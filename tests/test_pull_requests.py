import unittest
from unittest import mock
import os, sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from handlers.pull_requests import get_pull_requests


class TestAlbum(unittest.TestCase):

    @mock.patch('requests.get')
    def test_get_pull_request(self, mock_get):
        mock_get.return_value.json.return_value = [{"title": "title", "number": 3, "url": "url"}]
        expected_res = [{"title": "title", "num": 3, "link": "url"}]
        res = get_pull_requests()
        self.assertEqual(res, expected_res)
